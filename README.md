# Musica maestro!


## Specifica dei requisiti

Obiettivo dell'esercizio è *progettare* e *realizzare* un insieme di classi
che consentano di simulare un insieme di *strumenti musicali*, seguendo passo passo le specifiche di seguito riportate.

Prima di passare a uno step successivo assicurarsi con semplici test (usando JUnit) che quanto scritto funzioni.

#### Step 1
Implementare due diverse classi che rappresentano due diversi strumenti: `Trumpet` e `Horn`. Entrambe le classi implementano l'interfaccia `MusicalInstrument` e rispondono alla chiamata del metodo 
```
#!java
public String play()
```
che restituisce nel primo caso la stringa `"pepepe"` e nel secondo la stringa `"papapa"`.

#### Step 2
Implementare altre due classi `WaterGlass` e `IronRod`. Esse, pur NON implementando l'interfaccia `MusicalInstrument` hanno un comportamento molto simile: `WaterGlass` possiede un metodo
```
#!java
public String tap()
```
che restituisce la stringa `"diding"`; 

`IronRod` invece aderisce alla interfaccia `GermanPercussiveInstrument` e risponde quindi alla chiamata

```
#!java
public String spiel()
```
restituendo la stringa `"tatang"`.

#### Step 3
Aggiungere la possibilità di **usare** istanze della classe `WaterGlass` e delle classi aderenti alla interfaccia `GermanPercussiveInstrument` (e in particolare perciò della classe `IronRod`) come oggetti di tipo `MusicalInstrument`. Tale obiettivo deve essere raggiunto usando il *design pattern* denominato
**adapter**; più in dettaglio, è richiesta la realizzazione di due diversi adapter: `WaterGlassInstrument` che realizza un *Class adapter* e `GermanMusicInstrument` che realizza invece un *Object adapter*.


#### Step 4
Aggiungere la possibilità di **contare** il numero di volte che un qualsiasi oggetto di tipo `MusicalInstrument` suona. Tale obiettivo deve essere raggiunto usando il *design pattern* denominato
**decorator**; più in dettaglio, è richiesta la realizzazione di una classe `MusicalInstrumentCounter` che implementa l'interfaccia `MusicalInstrument` e *decora* un'istanza di `MusicalInstrument` in modo che venga conteggiato il numero di sonate emessi da una qualsiasi istanza decorata.
La classe `MusicalInstrumentCounter` deve implementare anche i metodi:
```
#!java
public static int getCount()
```
che restituisce il conteggio attuale;
```
#!java
public static void resetCount()
```
che resetta il conteggio attuale.


#### Step 5
Creare una classe che rappresenti un'orchestra di oggetti di tipo `MusicalInstrument`. Tale obiettivo deve essere raggiunto usando il *design pattern* denominato **composite**; più in dettaglio, è richiesta la realizzazione di una classe `Orchestra` che deve implementare l'interfaccia `MusicalInstrument` e deve rispondere all'invocazione del metodo `String play()` demandando la chiamata agli oggetti `MusicalInstrument` *aggregati*.
Le istanze della classe `Orchestra` devono consentire l'aggiunta di elementi tramite l'invocazione del metodo:
```
#!java
public void add(MusicalInstrument instrument)
```

